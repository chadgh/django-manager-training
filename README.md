# Instructions

After cloning the repo:

* `pip install -r requirements.pip`
* `git checkout step0`
* `python manage.py migrate`
* `python manage.py createsuperuser`

At this point you are ready to learn and explore. For each step in the repo do the following:

* `git checkout step[#]`
* `python manage.py migrate`
* open `example/models.py` and take a look.
* `python manage.py shell_plus` and play around with the models and their managers.

There are 7 steps starting at step0.


# Resources

* https://docs.djangoproject.com/en/1.10/topics/db/queries/
* https://docs.djangoproject.com/en/1.10/topics/db/managers/
* https://docs.djangoproject.com/en/1.10/topics/db/aggregation/
