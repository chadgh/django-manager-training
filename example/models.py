from django.db import models


class BookQuerySet(models.QuerySet):

    def not_archived(self):
        return self.exclude(archived=True)

    def archived(self):
        return self.filter(archived=True)

    def dickens(self):
        return self.filter(author='Charles Dickens')


class AuthorManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().annotate(book_count=models.Count('books'))


class Author(models.Model):
    name = models.CharField(max_length=255)

    authors = AuthorManager()
    objects = models.Manager()

    def __str__(self):
        return self.name

    @property
    def bcount(self):
        return self.books.count()

    class Meta:
        ordering = ['name']


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(Author, null=True, on_delete=models.SET_NULL,
                               related_name="books")
    year_published = models.IntegerField()
    archived = models.BooleanField(default=False)

    books = BookQuerySet.as_manager()
    objects = models.Manager()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['title']
