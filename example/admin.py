from django.contrib import admin

from .models import Author, Book


class ModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(Author, ModelAdmin)
admin.site.register(Book, ModelAdmin)
